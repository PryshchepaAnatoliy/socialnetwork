<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false"%>
<html>
<head>
    <title>SingUp</title>
</head>
<body>
    <form:form method="post"  modelAttribute="user">
        <h1> Регистрация </h1>
        <%--<p>--%>
            <%--<label for="usernamesignup" class="uname" data-icon="L">Ваш логин</label>--%>
            <%--<input id="usernamesignup" name="username" required="required" type="text" placeholder="mylogin" maxlength="12" />--%>
        <%--</p>--%>
        <p>
            <label for="emailsignup" class="youmail" data-icon="E" > Ваш email</label>
            <input id="emailsignup" name="email" required="required" type="email" placeholder="mylogin@mail.com"/>
        </p>
        <p>
            <label for="password" class="youpasswd" data-icon="P">Ваш пароль  </label>
            <input id="password" name="password" required="required" type="password" placeholder="123456"/>
            <%--<form:password id="password" path="password" required="required" type="password"/>--%>
            <%--placeholder="eg.X8df!90EO"/>--%>
        </p>

        <label for="userAvatar">Attach Image(JPG file only)</label>
        <form:input path="userAvatar" enctype="multipart/form-data" type="file" accept="jpg" /><br/>
        <%--<p>--%>
            <%--&lt;%&ndash;<label for="passwordsignup_confirm" class="youpasswd" data-icon="P">Пожалуйста, подтвердите пароль  </label>&ndash;%&gt;--%>
            <%--&lt;%&ndash;<form:password id="passwordsignup_confirm" path="confirmPassword" required="required" type="password" />&ndash;%&gt;--%>
                           <%--&lt;%&ndash;placeholder="eg. X8df!90EO"/>&ndash;%&gt;--%>
        <%--</p>--%>
        <p >
            <input type="submit" value="Регистрация"/>
        </p>
        <%--<div style="color:red"><form:errors path="*" cssClass="error" /></div>--%>
        <%--<p class="change_link">--%>
            <%--Вы уже зарегистрированы?--%>
            <%--<a href="<c:url value="login.jsp" />" class="to_register"> Тогда войдите </a>--%>
        <%--</p>--%>
</form:form>
</body>
</html>
