package com.geekhub.controllers;

import com.geekhub.entity.User;
import com.geekhub.entity.UserInfo;
import com.geekhub.servise.UserService;
import com.geekhub.servise.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@Controller
public class AppController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserInfoService userInfoService;




    @RequestMapping("/user_info")
    public String userInfoList(Map<String,Object> map){
        map.put("userInfo", new UserInfo());
        map.put("userInfoList", userInfoService.listUserInfo());
        return "user_info";
    }

    @RequestMapping(value = {"/", "/userList"})
    public String userList(Map<String, Object> map) {
        map.put("user", new User());
        map.put("userList", userService.listUser());
        return "userList";
    }

    @RequestMapping(value =  "/welcome**" , method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("hello");
        return model;

    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is protected page!");
        model.setViewName("admin");
        return model;
    }


    @RequestMapping(value = "/singup", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result) {
        if(result.hasErrors()) {
            return "singup";
        }
        userService.addUser(user);
        return "login";
    }

    @RequestMapping(value = "/singup", method = RequestMethod.GET)
    public String addUser(Map<String, Object> map, Model model,Principal principal) {
        model.addAttribute("user", new User());
        if (principal != null){
            map.put("username", principal.getName());
        }
        return "singup";
    }


    @RequestMapping(value = "/id{id}", method = RequestMethod.GET)
    public String app (Map<String, Object> map,Model model, @PathVariable("id") Integer id, User user, Principal principal) {

//        if (principal != null){
//            map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
//        }
        model.addAttribute("byId", userService.getUser(id));
        model.addAttribute("userInfo", userService.getUserInfo(id));
//        map.put("username", principal.getName());
        return "profile";
        }
//        model.addAttribute("byId", userService.getUser(id));
//        byte[] thumb = userService.getCurrentUserAvatar(id);

//        String name = "userAvatar";
//        response.setContentType("image/jpeg");
//        response.setContentLength(thumb.length);
//
//        response.setHeader("Content-Disposition", "inline; filename=\"" + name
//                + "\"");

//        BufferedInputStream input = null;
//        BufferedOutputStream output = null;
//
//        try {
//            input = new BufferedInputStream(new ByteArrayInputStream(thumb));
//           // output = new BufferedOutputStream(response.getOutputStream());
//            byte[] buffer = new byte[8192];
//            int length;
//            while ((length = input.read(buffer)) > 0) {
//                output.write(buffer, 0, length);
//            }
//        } catch (IOException e) {
//            System.out.println("There are errors in reading/writing image stream "
//                    + e.getMessage());
//        } finally {
//            if (output != null)
//                try {
//                    output.close();
//                } catch (IOException ignore) {
//                }
//            if (input != null)
//                try {
//                    input.close();
//                } catch (IOException ignore) {
//                }
//        }

//    @RequestMapping(value = "/savecomplain", method = RequestMethod.POST)
//    public String saveComplains(@ModelAttribute("complains") Complains complains, BindingResult result)
//    {
//        complainService.saveComplains(complains);
//        return "redirect:CrimeForumLogin";
//    }
//
//    @RequestMapping(value = "/images/{id}", method = RequestMethod.GET)
//    public String image (Map<String, Object> map,Model model, @PathVariable("id") Integer id, User user, Principal principal) {
//
//
//    }

}