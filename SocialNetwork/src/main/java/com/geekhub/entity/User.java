package com.geekhub.entity;


import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "USERS")
public class User {
    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Integer id;

//    @Column(name = "USERNAME",unique = true)
//    @Size(min=4, max=20,
//            message="Имя должно быть от 4 до 20 символов")
//    @Pattern(regexp="^[a-zA-Z0-9]+$",
//            message="Имя должно состоять из латинского алфавитаи не иметь пробелов")
//    private String username;

    @Column(name = "EMAIL")
    @Pattern(regexp="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}",
            message="Не правильно введен email адрес.")
    private String email;

    @Column(name = "PASSWORD")
    @Size(min=6, max=20,
            message="Пароль должет быть не меньше 6 символов")
    private String password;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "ROLE")
    private String role;

    @Lob
    @Column(name="USER_AVATAR", nullable=false, columnDefinition="mediumblob")
    private byte[] userAvatar;


    @OneToMany(mappedBy = "user",fetch=FetchType.EAGER)
    private List<UserInfo> userInfo;

    public List<UserInfo> getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(List<UserInfo> userInfo) {
        this.userInfo = userInfo;
    }

    //    @Column(name = "CONFIRMPASSWORD")
//    @Size(min=6, max=20,
//            message="Пароль должет быть не меньше 6 символов")
//    private String confirmPassword;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(byte[] userAvatar) {
        this.userAvatar = userAvatar;
    }
}
