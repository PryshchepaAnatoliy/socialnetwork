package com.geekhub.dao;

import java.util.List;

import com.geekhub.entity.User;
import com.geekhub.entity.UserInfo;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addUser(User user){
        sessionFactory.getCurrentSession().save(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUser() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    public void removeUser(Integer id) {
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
        }
    }

    public User getUser(int id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }


    public byte[] getCurrentUserAvatar(int id) {
//        return sessionFactory.getCurrentSession().get(User.class, id);
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("id", id)).uniqueResult();
        return user.getUserAvatar();
//        sessionFactory.getCurrentSession().g
    }

    public List<UserInfo> getUserInfo(Integer id) {
        User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
        return user.getUserInfo();
    }
}

