package com.geekhub.dao;


import com.geekhub.entity.UserInfo;

import java.util.List;

public interface UserInfoDAO {
    public List<UserInfo> listUserInfo();

}
