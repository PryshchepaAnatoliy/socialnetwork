package com.geekhub.dao;

import com.geekhub.entity.UserInfo;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserInfoDAOImpl implements UserInfoDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<UserInfo> listUserInfo() {
        return sessionFactory.getCurrentSession().createQuery("from UserInfo").list();
    }



}
