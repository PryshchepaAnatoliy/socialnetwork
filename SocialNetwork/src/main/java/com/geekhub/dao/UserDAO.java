package com.geekhub.dao;

import com.geekhub.entity.User;
import com.geekhub.entity.UserInfo;

import java.util.List;


public interface UserDAO {
    public void addUser(User user);

    public List<User> listUser();

    public void removeUser(Integer id);

    public User getUser(int id);

    public byte[] getCurrentUserAvatar(int id);

    public List <UserInfo> getUserInfo(Integer id);


}