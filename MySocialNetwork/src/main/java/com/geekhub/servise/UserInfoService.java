package com.geekhub.servise;

import com.geekhub.entity.UserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public interface UserInfoService {
    public List<UserInfo> listUserInfo();
}
