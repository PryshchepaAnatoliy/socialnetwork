package com.geekhub.servise;

import com.geekhub.dao.UserInfoDAO;
import com.geekhub.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDAO userInfoDAO;

    @Transactional
    public List<UserInfo> listUserInfo() {
        return userInfoDAO.listUserInfo();
    }

}
