package com.geekhub.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "NOTE_COMMENT")
public class NoteComment {
    @Id
    @Column(name = "COMMENT_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DATE_OF_COMMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfComment;


    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="note_id")
    private UserNote userNote;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfComment() {
        return dateOfComment;
    }

    public void setDateOfComment(Date dateOfComment) {
        this.dateOfComment = dateOfComment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserNote getUserNote() {
        return userNote;
    }

    public void setUserNote(UserNote userNote) {
        this.userNote = userNote;
    }
}
