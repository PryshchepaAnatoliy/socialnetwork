package com.geekhub.servise;

import com.geekhub.entity.NoteComment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public interface NoteCommentService {
    public void addNoteComment(NoteComment noteComment);
    public NoteComment getNoteComment(Integer id);
}
