package com.geekhub.servise;


import com.geekhub.dao.UserDAO;
import com.geekhub.dao.UserNoteDAO;
import com.geekhub.entity.NoteComment;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserNoteServiceImpl implements UserNoteService{

    @Autowired
    private UserNoteDAO userNoteDAO;

    @Transactional
    public void addUserNote(UserNote userNote) {
        userNoteDAO.addUserNote(userNote);
    }

    @Transactional
    public UserNote getUserNote(Integer id) {
        return userNoteDAO.getUserNote(id);
    }

    @Transactional
    public List<UserNote> listUserNote() {
        return userNoteDAO.listUserNote();
    }

    @Transactional
    public List<NoteComment> getNoteComment(Integer id){
        return userNoteDAO.getNoteComment(id);
    }


}
