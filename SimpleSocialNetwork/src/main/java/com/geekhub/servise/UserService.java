package com.geekhub.servise;


import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public interface UserService {

    public void addUser(User user);
    public List<User> listUser();
    public void removeUser(Integer id);
    public User getUser(Integer id);

    public List<UserNote> getUserNote(Integer id);

    public byte[] getCurrentUserAvatar(Integer id);
    public Integer getIdByUsername(String username);
    public void updateUser(User user);
//    public List<User> listUser();

//    public void setRoleAdmin(Integer id);
//    public void setRoleUser(Integer id);
//    public void setBan(Integer id);
//    public void setUnban(Integer id);

}
