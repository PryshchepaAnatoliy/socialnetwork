package com.geekhub.servise;


import com.geekhub.dao.UserDAO;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Transactional
    public void addUser(User user) {
        user.setRole("ROLE_USER");
        user.setEnabled(true);
        userDAO.addUser(user);
    }

    @Transactional
    public List<User> listUser() {
        return userDAO.listUser();
    }

    @Transactional
    public void removeUser(Integer id) {
        userDAO.removeUser(id);
    }

    @Transactional
    public User getUser(Integer id) {
        return userDAO.getUser(id);
    }

    @Transactional
    public byte[] getCurrentUserAvatar(Integer id) {
        return userDAO.getCurrentUserAvatar(id);
    }

    @Transactional
    public List<UserNote> getUserNote(Integer id){
        return userDAO.getUserNote(id);
    }

//    @Transactional
//    public List<UserNote> getUserNote(Integer id) {
//        return userDAO.getUserNote(id);
//    }
    @Transactional
    public Integer getIdByUsername(String username) {
        return userDAO.getIdByUsername(username);
    }

    @Transactional
    public void updateUser(User user){
        userDAO.updateUser(user);
    }


}