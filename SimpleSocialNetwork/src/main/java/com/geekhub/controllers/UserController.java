package com.geekhub.controllers;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import com.geekhub.servise.NoteCommentService;
import com.geekhub.servise.UserNoteService;
import com.geekhub.servise.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserNoteService userNoteService;

    @Autowired
    private NoteCommentService noteCommentService;

    @RequestMapping(value =  "/welcome**" , method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("helloworld");
        return model;

    }

    @RequestMapping(value =  "/page**" , method = RequestMethod.GET)
    public String page() {

        return "page";
    }
    @RequestMapping(value =  "/page1**" , method = RequestMethod.GET)
    public String page1() {

        return "page1";
    }


    @RequestMapping(value="/editUser{id}", method=RequestMethod.GET)
    public ModelAndView updateUser(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("profile_settings");
        User user = userService.getUser(id);
        modelAndView.addObject("user",user);
        return modelAndView;
    }

    @RequestMapping(value="/editUser{id}", method=RequestMethod.POST)
    public ModelAndView editingForm(Map<String, Object> map, Model model, @ModelAttribute User user, @PathVariable Integer id, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("redirect:/profile_settings");
//        ModelAndView modelAndView1 = new ModelAndView("redirect:/login");
//        if (principal != null){
//            map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
//        }
//        int sessionUserId = userService.getIdByUsername(principal.getName());
//        model.addAttribute("byId", userService.getUser(id));
//        if(id.equals(sessionUserId)){
        userService.updateUser(user);
            return modelAndView;
//        }else
//        return modelAndView1;
    }

    @RequestMapping("/user_info")
    public String userEditing(Map<String, Object> map,Principal principal) {
        map.put("user", new User());
        map.put("userList", userService.listUser());
        if (principal != null) {
            map.put("username", SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "user_info";
    }

    @RequestMapping(value = "/id{id}", method = RequestMethod.GET)
    public String userProfile(Map<String, Object> map,Model model, @PathVariable("id") Integer id, User user, Principal principal) {
//        if(userService.getUser(id).equals(null)){
//            return "helloworld";
//        }else {

//            if (principal != null){
//                map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
//            }
            model.addAttribute("byId", userService.getUser(id));
            model.addAttribute("userNote", userService.getUserNote(id));
//            model.addAttribute("noteComment", userNoteService.getNoteComment(userNoteService.getUserNote(id).getId()));
//            for(UserNote userNote : userService.getUserNote(id)){
//                model.addAttribute("noteComment"+userNote.getId(), userNoteService.getNoteComment(userNote.getId()));
//
//                System.out.println("note id "+userNote.getId());
//                System.out.println("getNoteComment "+userNoteService.getNoteComment(userNote.getId()));
//            }

            System.out.println(userNoteService.getNoteComment(userNoteService.getUserNote(id).getId()));
//      map.put("username", principal.getName());
            return "profile";
//        }
    }

    @RequestMapping(value = "/id{id}", method = RequestMethod.POST)
    public String addNoteComment(@PathVariable("id") Integer id,@Valid @ModelAttribute("noteComment") NoteComment noteComment, Principal principal) {
        if(userService.getUser(id).equals(null)){
            return "helloworld";
        }else {
            int sessionUserId = userService.getIdByUsername(principal.getName());
            Date date = new Date();
            noteComment.setDateOfComment(date);
            noteComment.setUser(userService.getUser(sessionUserId));

//            noteComment.setUserNote(userService.getUserNote(id));
//            noteComment.setUserWhoseNote(userService.getUser(sessionUserId));
            noteCommentService.addNoteComment(noteComment);
            return "redirect:/id{id}";
        }
    }

//    public String addNote(@PathVariable("id") Integer id,@Valid @ModelAttribute("userNote") UserNote userNote, Principal principal){
//        int sessionUserId = userService.getIdByUsername(principal.getName());
//        Date date = new Date();
//        userNote.setDateOfNote(date);
//        userNote.setUserWhoseNote(userService.getUser(sessionUserId));
//        userNote.setUser(userService.getUser(id));
//
//        userNoteService.addUserNote(userNote);
//        return "redirect:/id{id}";
//    }


}
