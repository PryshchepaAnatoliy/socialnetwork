package com.geekhub.controllers;


import com.geekhub.entity.NoteComment;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import com.geekhub.servise.NoteCommentService;
import com.geekhub.servise.UserNoteService;
import com.geekhub.servise.UserService;
import com.geekhub.validator.SignupValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.Map;


@Controller
public class AppController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserNoteService userNoteService;

    @Autowired
    private NoteCommentService noteCommentService;


    @RequestMapping(value = {"/", "/userList"})
    public String userList(Map<String, Object> map) {
        map.put("user", new User());
        map.put("userList", userService.listUser());
        return "userList";
    }

//    @RequestMapping(value =  "/welcome**" , method = RequestMethod.GET)
//    public ModelAndView welcomePage() {
//
//        ModelAndView model = new ModelAndView();
//        model.addObject("title", "Spring Security Hello World");
//        model.addObject("message", "This is welcome page!");
//        model.setViewName("helloworld");
//        return model;
//
//    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is protected page!");
        model.setViewName("admin");
        return model;
    }


    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result) {
        if(result.hasErrors()) {
            return "signup";
        }
        userService.addUser(user);
        return "login";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String addUser(Map<String, Object> map, Model model,Principal principal) {
        model.addAttribute("user", new User());
        if (principal != null){
            map.put("username", principal.getName());
        }
        return "signup";
    }




//    @RequestMapping(value = "/id{id}", method = RequestMethod.POST)
//    public String addUserNote(@PathVariable("id") Integer id,@Valid @ModelAttribute("userNote") UserNote userNote, Principal principal) {
//        if(userService.getUser(id).equals(null)){
//            return "helloworld";
//        }else {
//            int sessionUserId = userService.getIdByUsername(principal.getName());
//
////        UserNote userNote = new UserNote();
////        model.addAttribute("userNote", userNote);
//            Date date = new Date();
//
//            userNote.setDateOfNote(date);
//            userNote.setUser(userService.getUser(id));
//            userNote.setUserWhoseNote(userService.getUser(sessionUserId));
////        userNote.setDescription();
//            userNoteService.addUserNote(userNote);
//            return "redirect:/id{id}";
//        }
//    }




    @RequestMapping("/images/{id}")
    public void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Integer id) throws Exception{
//        byte[] thumb =
        byte[] bytes = userService.getCurrentUserAvatar(id);

        response.setContentType("image/png"); //MIME-TYPE



        ServletOutputStream out = response.getOutputStream();
        out.write(bytes);
        out.flush();
    }




    @RequestMapping(value = "/logout")
    public String logout() {
        return "index";
    }

//        String name = "userAvatar";
////        response.setContentType("/image/");
//        response.setContentLength(thumb.length);
//        response.setHeader("Content-Disposition", "inline; filename=\"" + name
//                + "\"");
//
//        BufferedInputStream input = null;
//        BufferedOutputStream output = null;
//

//        model.addAttribute("byId", userService.getUser(id));
//        byte[] thumb = userService.getCurrentUserAvatar(id);

//        String name = "userAvatar";
//        response.setContentType("image/jpeg");
//        response.setContentLength(thumb.length);
//
//        response.setHeader("Content-Disposition", "inline; filename=\"" + name
//                + "\"");

//        BufferedInputStream input = null;
//        BufferedOutputStream output = null;
//
//        try {
//            input = new BufferedInputStream(new ByteArrayInputStream(thumb));
//            output = new BufferedOutputStream(response.getOutputStream());
//            byte[] buffer = new byte[8192];
//            int length;
//            while ((length = input.read(buffer)) > 0) {
//                output.write(buffer, 0, length);
//            }
//        } catch (IOException e) {
//            System.out.println("There are errors in reading/writing image stream "
//                    + e.getMessage());
//        } finally {
//            if (output != null)
//                try {
//                    output.close();
//                } catch (IOException ignore) {
//                }
//            if (input != null)
//                try {
//                    input.close();
//                } catch (IOException ignore) {
//                }
//        }

//    @RequestMapping(value = "/savecomplain", method = RequestMethod.POST)
//    public String saveComplains(@ModelAttribute("complains") Complains complains, BindingResult result)
//    {
//        complainService.saveComplains(complains);
//        return "redirect:CrimeForumLogin";
//    }
//
//    @RequestMapping(value = "/images/{id}", method = RequestMethod.GET)
//    public String image (Map<String, Object> map,Model model, @PathVariable("id") Integer id, User user, Principal principal) {
//
//
//    }

}