package com.geekhub.controllers;

import com.geekhub.entity.User;
import com.geekhub.validator.SignupValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/signup1")
public class SignupController {
    @Autowired
    private SignupValidator signupValidator;

    @RequestMapping(method = RequestMethod.GET)
    public String signup(ModelMap model) {
        User signupForm = new User();
        model.put("signupForm", signupForm);
        return "signup1";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processSignup(User signupForm, BindingResult result) {
        signupValidator.validate(signupForm, result);
        if (result.hasErrors()) {
            return "signup1";
        }
        return "signup-success";
    }
}