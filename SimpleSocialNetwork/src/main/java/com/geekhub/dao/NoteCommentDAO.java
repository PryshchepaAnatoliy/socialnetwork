package com.geekhub.dao;


import com.geekhub.entity.NoteComment;

public interface NoteCommentDAO {
    public void addNoteComment(NoteComment noteComment);
    public NoteComment getNoteComment(Integer id);
}
