package com.geekhub.dao;


import com.geekhub.entity.NoteComment;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoteCommentDAOImpl implements NoteCommentDAO{

@Autowired
private SessionFactory sessionFactory;

    public NoteComment getNoteComment(Integer id) {
        return (NoteComment) sessionFactory.getCurrentSession().get(NoteComment.class, id);
    }
    public void addNoteComment(NoteComment noteComment){
        sessionFactory.getCurrentSession().save(noteComment);
    }


}
