package com.geekhub.dao;


import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addUser(User user){
        sessionFactory.getCurrentSession().save(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUser() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    public void removeUser(Integer id) {
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
        }
    }

    public User getUser(Integer id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }


    public byte[] getCurrentUserAvatar(int id) {
//        return sessionFactory.getCurrentSession().get(User.class, id);
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("id", id)).uniqueResult();
        return user.getUserAvatar();
//        sessionFactory.getCurrentSession().g
    }

    public List<UserNote> getUserNote(Integer id){
        User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
        return user.getUserNote();
    }



    public Integer getIdByUsername(String username) {
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", username)).uniqueResult();
        return user.getId();
    }

    public List<User> searching(String firstName, String city, String country, String patronymic) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);

        return criteria.add(Restrictions.eq("firstName",firstName)).list();
    }

    @Override
    public void updateUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        User userToUpdate = (User) session.get(User.class, user.getId());
        userToUpdate.setId(user.getId());
        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setLastName(user.getLastName());
        userToUpdate.setCity(user.getCity());
        userToUpdate.setCountry(user.getCountry());
        userToUpdate.setPhone(user.getPhone());
        userToUpdate.setGender(user.getGender());
        userToUpdate.setAddress(user.getAddress());
        session.save(userToUpdate);
    }


//
}

