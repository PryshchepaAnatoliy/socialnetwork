package com.geekhub.dao;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.UserNote;
import com.geekhub.entity.UserNote;

import java.util.List;

public interface UserNoteDAO {
    public void addUserNote(UserNote userNote);
    public UserNote getUserNote(Integer id);
    public List<UserNote> listUserNote();
    public List<NoteComment> getNoteComment(Integer id);
//    public Integer getIdByUsername(String username);
}
