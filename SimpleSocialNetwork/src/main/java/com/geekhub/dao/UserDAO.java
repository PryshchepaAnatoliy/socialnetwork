package com.geekhub.dao;

import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;

import java.util.List;


public interface UserDAO {

    public void addUser(User user);
    public List<User> listUser();
    public void removeUser(Integer id);
    public User getUser(Integer id);
    public byte[] getCurrentUserAvatar(int id);
    public Integer getIdByUsername(String username);
    public List<UserNote> getUserNote(Integer id);
    public List<User> searching(String firstName, String city,String country, String patronymic);
    public void updateUser(User user);


}