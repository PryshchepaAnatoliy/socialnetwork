<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Blog Post - Start Bootstrap Template</title>

  <!-- Bootstrap Core CSS -->
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <%--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>--%>
  <!-- Custom CSS -->
  <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]
  <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Start Bootstrap</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="#">About</a>
        </li>
        <li>
          <a href="#">Services</a>
        </li>
        <li>
          <a href="#">Contact</a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>
<%--<div class="row">--%>
  <%--<div class="col-sm-10"><h1>User name</h1></div>--%>
  <%--<div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>--%>
<%--</div>--%>
<div class="row">

    </div>

<!-- Page Content -->
<div class="container">

  <div class="row">
    <%--<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">--%>
      <%--<ul class="nav">--%>
        <%--<li class="active"><a href="./Bootply.com - Bootstrap Collapsing sidebar drawer menu_files/Bootply.com - Bootstrap Collapsing sidebar drawer menu.html">Home</a></li>--%>
        <%--<li><a href="./Bootply.com - Bootstrap Collapsing sidebar drawer menu_files/Bootply.com - Bootstrap Collapsing sidebar drawer menu.html">Link 1</a></li>--%>
        <%--<li><a href="./Bootply.com - Bootstrap Collapsing sidebar drawer menu_files/Bootply.com - Bootstrap Collapsing sidebar drawer menu.html">Link 2</a></li>--%>
        <%--<li><a href="./Bootply.com - Bootstrap Collapsing sidebar drawer menu_files/Bootply.com - Bootstrap Collapsing sidebar drawer menu.html">Link 3</a></li>--%>
      <%--</ul>--%>
    <%--</div>--%>
    <div class="col-sm-3"><!--left col-->
      <ul class="nav">
        <li><a href="#">Моя страница</a></li>
        <li><a href="#">Мои друзья</a></li>
        <li><a href="#">Мои настройки</a></li>
        <li><a href="#">Link 3</a></li>
      </ul>
      <ul class="list-group" class="nav">
        <li class="list-group-item text-muted"class="active">Profile</li>
        <%--<li><a href="#">Link</a></li>--%>
        <li class="list-group-item text-right" ><span class="pull-left"><strong><a href="#">Link</a></strong></span></li>
        <li class="list-group-item text-right"><span class="pull-left"><strong><a href="#">Link</a></strong></span>123</li>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Joined</strong></span> 2.13.2014</li>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Last seen</strong></span> Yesterday</li>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Real name</strong></span> Joseph Doe</li>

      </ul>


      <div class="panel panel-default">
        <div class="panel-heading"><strong><a href="#">Link</a> </strong><i class="fa fa-link fa-1x"></i></div>
        <div class="panel-body"><a href="http://bootnipets.com">bootnipets.com</a></div>
      </div>
      </div>
    <!-- Blog Post Content Column -->
    <div class="col-lg-8">
      <div class="row">
        <%--<div id="details" class="span7"></div>--%>
        <div class="span5">
          <div id="reports" class="well">
            <h3 class="modal-header">Direct Reports</h3>
            <%--<div class="alert alert-info no-reports">--%>
              <%--<h4 class="alert-heading">Info</h4>--%>

            <%--</div>--%>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                    <tr>
                      <td>Department:</td>
                      <td>Programming</td>
                    </tr>
                    <tr>
                      <td>Hire date:</td>
                      <td>06/23/2013</td>
                    </tr>
                    <tr>
                      <td>Date of Birth</td>
                      <td>01/24/1988</td>
                    </tr>

                    <tr>
                    <tr>
                      <td>Gender</td>
                      <td>Male</td>
                    </tr>
                    <tr>
                      <td>Home Address</td>
                      <td>Metro Manila,Philippines</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td><a href="mailto:info@support.com">info@support.com</a></td>
                    </tr>
                    <td>Phone Number</td>
                    <td>123-4567-890(Landline)<br><br>555-4567-890(Mobile)
                    </td>

                    </tr>

                    </tbody>
                  </table>

                  <a href="#" class="btn btn-primary">My Sales Performance</a>
                  <a href="#" class="btn btn-primary">Team Sales Performance</a>
                </div>
              </div>
            </div>
            <h1 class="modal-header"></h1>
            <%--<div class="well">--%>
            <button type="button" class="btn btn-mini" onclick="$('#target').toggle();">Оставить запись</button>
            <div id="target" style="display: none">
              <form role="form">
                <div class="form-group">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>

            <div class="media">
              <a class="pull-left" href="#">
                <img class="media-object" src="http://placehold.it/64x64" alt="">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Start Bootstrap
                  <small>August 25, 2014 at 9:30 PM</small>
                </h4>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                <button type="button" class="btn-link" onclick="$('#target1').toggle();">Комментировать</button>
                <div id="target1" style="display: none">
                  Hide show.....
                </div>
              </div>

            </div>
            </div>

          <%--</div>--%>
        </div>
      </div>
      <!-- Comments Form -->
      <div class="well">
        <h4>Leave a Comment:</h4>
        <form role="form">
          <div class="form-group">
            <textarea class="form-control" rows="3"></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <hr>

      <!-- Posted Comments -->

      <!-- Comment -->
      <div class="media">
        <a class="pull-left" href="#">
          <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
          <h4 class="media-heading">Start Bootstrap
            <small>August 25, 2014 at 9:30 PM</small>
          </h4>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </div>
      </div>

      <!-- Comment -->
      <div class="media">
        <a class="pull-left" href="#">
          <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
          <h4 class="media-heading">Start Bootstrap
            <small>August 25, 2014 at 9:30 PM</small>
          </h4>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          <!-- Nested Comment -->
          <div class="media">
            <a class="pull-left" href="#">
              <img class="media-object" src="http://placehold.it/64x64" alt="">
            </a>
            <div class="media-body">
              <h4 class="media-heading">Nested Start Bootstrap
                <small>August 25, 2014 at 9:30 PM</small>
              </h4>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
          </div>
          <!-- End Nested Comment -->
        </div>
      </div>

    </div>

    <!-- Blog Sidebar Widgets Column -->
    <div class="col-md-4">

      <!-- Blog Search Well -->
      <div class="well">
        <h4>Blog Search</h4>
        <div class="input-group">
          <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
        </div>
        <!-- /.input-group -->
      </div>

    </div>

  </div>
  <!-- /.row -->

  <hr>

  <!-- Footer -->
  <footer>
    <div class="row">
      <div class="col-lg-12">
        <p>Copyright &copy; Your Website 2014</p>
      </div>
    </div>
    <!-- /.row -->
  </footer>

</div>
<!-- /.container -->

<!-- jQuery -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>

</body>

</html>
