<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>${byId.firstName} ${byId.patronymic}</title>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Start Bootstrap</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>





        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="nav">
                        <li><a href="id${username}">Моя страница</a></li>
                        <li><a href="#">Мои друзья</a></li>
                        <li><a href="#">Мои настройки</a></li>
                        <li><a href="#">Link 3</a></li>
                    </ul>
                </div>



                <div class="col-lg-8">
                <div class="row">
                    <div class="span5">
                        <div id="reports" class="well">
                            <h3 class="modal-header">${byId.firstName} ${byId.lastName} ${byId.patronymic}</h3>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="${pageContext.request.contextPath}/resources/images/photo.png" class="img-circle"> </div>
                                    <div class=" col-md-9 col-lg-9 ">
                                        <table class="table table-user-information">
                                            <tbody>
                                            <tr>
                                                <td>Адрес:</td>
                                                <td>${byId.address}</td>
                                            </tr>
                                            <tr>
                                                <td>Город:</td>
                                                <td>${byId.city}</td>
                                            </tr>
                                            <tr>
                                                <td>Страна:</td>
                                                <td>${byId.country}</td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td>День рождения:</td>
                                                <td>${byId.birthday}</td>
                                            </tr>
                                            <tr>
                                                <td>Стать:</td>
                                                <td>${byId.gender}</td>
                                            </tr>
                                                <td>Телефон:</td>
                                                <td>${byId.phone}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <a href="editUser${byId.id}">Изменить</a>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn-link" onclick="$('#wall').toggle();">Просмотреть все записи</button>
                            <div id="wall" style="display: none">

                            <h1 class="modal-header"></h1>
                            <button type="button" class="btn btn-mini" onclick="$('#target').toggle();">Оставить запись</button>
                            <div id="target" style="display: none">
                                <form:form role="form"  method="post"  modelAttribute="userNote">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="description" required="required"type="text"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Отправить</button>
                                </form:form>
                            </div>
                            <c:forEach items="${userNote}" var="userNote">

                                <div class="well">
                            <div class="media">

                                <a class="pull-left" href="#">
                                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                                </a>

                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="id${userNote.userWhoseNote.getId()}">${userNote.userWhoseNote.getFirstName()} ${userNote.userWhoseNote.getPatronymic()}</a>
                                        <small>${userNote.dateOfNote}</small>
                                    </h4>
                                    ${userNote.description}
                                </div><button type="button" class="btn-link" onclick="$('#target${userNote.id}').toggle();">Комментировать</button>

                                    <div id="target${userNote.id}" style="display: none">
                                        <form:form role="form"  method="post"  modelAttribute="noteComment">
                                            <div class="form-group">
                                                <textarea class="form-control" rows="3" name="description" required="required"type="text"></textarea>

                                                <input name="userNote" type="hidden" id="hidden" value="note">
                                                <%--<textarea class="form-control" rows="3" name="userNote" required="required"type="text">${userNote.id}</textarea>--%>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Коментировать</button>
                                        </form:form>
                                        <c:catch var="noteid">${userNote.id}</c:catch>

                                        <c:set value="noteComment${userNote.id}" var="1234"/>
                                        <c:out value="${noteid}"/>
                                        <c:forEach items="${noteComment1}" var="noteComment1">
                                            <c:choose>

                                             <c:when test="${userNote.id == noteComment1.userNote.getId()}">
                                                <c:out value="${userNote.id}"/>
                                                <p>noteComment.id: ${noteComment1.id}</p>
                                                <p>noteComment.description :${noteComment1.description}</p>
                                                <p>date :${noteComment1.dateOfComment}</p>
                                                <p>name :${noteComment1.user.getFirstName()}</p>
                                             </c:when>
                                             <c:when test="${empty noteComment1}">
                                                    fg
                                             </c:when>
                                            </c:choose>
                                        </c:forEach>
                                                  </div>

                                              </div>
                                              </div>

                                              </c:forEach>
                                              </div>
                                          </div>

                                          <%--</div>--%>
                    </div>
                </div>
                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
    <sec:authorize access="isAuthenticated()">
        <a href="<c:url value="/logout"/>">Logout</a>
    </sec:authorize>
    <p>email: ${byId.email}</p>
    <p>password :${byId.password}</p>
    <p>password :${byId.birthday}</p>
    <p>password :${byId.firstName}</p>
    <p>Фамилия ${byId.patronymic}</p>
    ${byId.id}
    <img src="${byId.userAvatar}"  height="75px" width="75px" align="left"/>
    <img src="image/jpeg?id=${byId.id}" height="75px" width="75px" align="left" />
    <img src="<c:url value="/images/jpeg/${byId.id}"/>" height="75px" width="75px" align="left"/>
    <%--<c:catch var="idUserInfo">${userInfo.id}</c:catch>--%>
    <c:choose>
        <c:when test="${empty userNote }">null
            <br />
        </c:when>

        <c:otherwise>
            <c:forEach items="${userNote}" var="userNote">
                <p>id: ${userNote.id}</p>
                <p>description :${userNote.description}</p>
                <p>date :${userNote.dateOfNote}</p>
                    <c:forEach items="${noteComment}" var="noteComment">
                        <p>idsadas: ${noteComment.id}</p>
                        <p>description :${noteComment.description}</p>
                        <p>date :${noteComment.dateOfComment}</p>
                    </c:forEach>
            </c:forEach>
234

        </c:otherwise>


    </c:choose>
    <%--<c:forEach items="${userInfo}" var="userInfo">--%>
        <%--<p>city: ${userInfo.id}</p>--%>
        <%--<p>country :${userInfo.country}</p>--%>
    <%--</c:forEach>--%>



                    <%--  <form:form method="post"  modelAttribute="userNote">
                          <p>
                              <label for="description"> Описсание</label>
                              <input id="description" name="description" required="required" type="text" />
                          </p>
                          <input type="submit" value="Написать"/>
                      </form:form>
                  <%--<c:if test="${salary > 2000} "> </c:if>--%>
    <%--<c:if ">--%>
    <%--<p>My salary is: <c:out value="${salary}"/><p>--%>
        <%--</c:if>--%>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
    </body>
</html>