<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<c:url var="actionUrl" value="savecomplain" />
<form:form id="complain-form" commandName="complains" method="post" action="${actionUrl }" class="form-signin" >
  <fieldset>

    <%--<form:input path="complainId" type="hidden" /><br/>--%>
    <%--<form:input path="username" type="hidden" value="${username}"/>--%>

    <label for="subject">Subject</label>
    <form:input path="subject" id="subject" name="subject" placeholder="What happened?" type="text" />


    <label for="placeOfIncident">Incident Place</label>
    <form:select path="placeOfIncident" type="placeOfIncident" >
      <option>Bangkok</option>
      <option>Tokyo</option>
      <option>Manila</option>
    </form:select>

    <label for="category">Category</label>
    <form:select path="category" type="category" >
      <option>Robbery</option>
      <option>Theft</option>
      <option>Carnapping</option>
      <option>Domestic Abuse</option>
      <option>Others</option>
    </form:select>

    <label for="password">Date Of Incident</label>
    <form:input path="dateOfCrime" id="dateOfCrime" name="dateOfCrime" placeholder="YYYY-MM-DD" class="datepicker" />

    <label for="complainDetail">Details</label>
    <form:textarea path="complainDetail" id="complainDetail" colspan="100" name="complainDetail" rows="10" cols="70"></form:textarea>

    <label for="complainImage">Attach Image(JPG file only)</label>
    <form:input path="complainImage" enctype="multipart/form-data" type="file" accept="jpg" /><br/>

    <button class="btn btn-success btn-large" type="submit" id="register">Submit</button>
    <button type="reset" class="btn">Reset</button>

  </fieldset>
</form:form>
</body>
</html>
