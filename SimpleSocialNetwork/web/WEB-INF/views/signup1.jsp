<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
  <title>Sign Up</title>
  <style type="text/css">
    span.error {
      color: red;
    }
  </style>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">
</head>
<body>
<h1>Sign Up</h1>

<form:form method="post" commandName="signupForm">
  <table>
    <tr>
      <td>Username:</td>
      <td><input path="firstName" /></td>
      <td><span class="error"><form:errors path="firstName" cssClass="error" /></span></td>
    </tr>

    <tr>
      <td>Password:</td>
      <td><input path="password" type="password" /></td>
      <td><span class="error"><form:errors path="password" /></span></td>
    </tr>

    <tr>
      <td>Confirm Password:</td>
      <td><input password path="confirmPassword" type="password"  /></td>
      <td><span class="error"><form:errors
              path="confirmPassword" /></span></td>
    </tr>

    <tr>
      <td>Email:</td>
      <td><input path="email" /></td>
      <td><span class="error"><form:errors path="email" /></span></td>
    </tr>

    <tr>
      <td colspan="3"><input type="submit" value="Submit" /></td>
    </tr>
  </table>
</form:form>

<a href="${pageContext.request.contextPath}/" title="Home">Home</a>

<br><br>
<div class="container-fluid well span6">
  <div class="row-fluid">
    <div class="span2" >
      <img src="https://secure.gravatar.com/avatar/de9b11d0f9c0569ba917393ed5e5b3ab?s=140&r=g&d=mm" class="img-circle">
    </div>

    <div class="span8">
      <h3>User Name</h3>
      <h6>Email: MyEmail@servidor.com</h6>
      <h6>Ubication: Colombia</h6>
      <h6>Old: 1 Year</h6>
      <h6><a href="#">More... </a></h6>
    </div>

    <div class="span2">
      <div class="btn-group">
        <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
          Action
          <span class="icon-cog icon-white"></span><span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="#"><span class="icon-wrench"></span> Modify</a></li>
          <li><a href="#"><span class="icon-trash"></span> Delete</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</body>
</html>